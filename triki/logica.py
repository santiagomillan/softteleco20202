from graficos import mostrar_tablero , grafico_empate
import os
green = '\033[0;32m'
red = '\033[91m'
orange='\033[33m'
yellow = '\033[93m' 
col='\033[96m'
nombre1= input(orange +"nombre del jugador: ")
nombre2= input(orange +"nombre del jugador: ")
jugador1= " "
jugador2= " "

#este es el primer paso. tambien ejecuta el juego# 
def eleccion_ficha(tablero):
    '''el jugador 1 selecciona si quiere iniciar o no '''
    print(yellow + f" {nombre1} elige  O/X")
    print(yellow +" sale la ficha O")
    ficha = " "
    '''mientras no se meta un dato valido va a pedir la ficha '''
    while ficha != "O" and ficha != "X":
        ficha = input("----->").upper()
    '''si el jugdor uno es o sale primero '''
    if ficha == "O":
        jugador1 = "O" 
        jugador2 = "X"
        print(nombre1 , jugador1)
        print(nombre2 , jugador2)
        ''' se ejecuta 8 veces y si no hay ganador se declara empate  '''
        mostrar_tablero(tablero)
        movimiento_jugadorO(tablero, nombre1)
        movimiento_jugadorx(tablero, nombre2)
        movimiento_jugadorO(tablero, nombre1)
        movimiento_jugadorx(tablero, nombre2)
        movimiento_jugadorO(tablero, nombre1)
        movimiento_jugadorx(tablero, nombre2)
        movimiento_jugadorO(tablero, nombre1)
        movimiento_jugadorx(tablero, nombre2)
        movimiento_jugadorO(tablero, nombre1)
        print(yellow+ "EMPATE")
        grafico_empate()
        '''para poder salir del juego'''
        juegonuev()
        exit(0)
    else:
        '''si el jugdor uno es x sale de segundo '''
        jugador1 = "X"
        jugador2 =  "O"
        print(nombre2 , jugador2)
        print(nombre1 , jugador1)
        """agregar logica"""
        mostrar_tablero(tablero)
        movimiento_jugadorO(tablero, nombre2)
        movimiento_jugadorx(tablero, nombre1)
        movimiento_jugadorO(tablero, nombre2)
        movimiento_jugadorx(tablero, nombre1)
        movimiento_jugadorO(tablero, nombre2)
        movimiento_jugadorx(tablero, nombre1)
        movimiento_jugadorO(tablero, nombre2)
        movimiento_jugadorx(tablero, nombre1)
        movimiento_jugadorO(tablero, nombre2)
        print(yellow + "EMPATE")
        grafico_empate()
        juegonuev()
        exit(0)
    return jugador1, jugador2 


def movimiento_jugador(tablero, nombre):
    ''' devuelve  la casilla del jugador/ se tiene arreglo con las posibles posociones  '''
    posiciones = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    posicion = None
    while True:
        if posicion not in posiciones:
            posicion = input(orange+ f"{nombre} te toca (1-9) ")
        else:
            posicion = int(posicion)
            if not casilla_libre(tablero , posicion-1):
                print("esa posicion esta ocupada")
            else:
                return posicion-1

'''
def movimiento_jugador1(tablero, nombre):
    posiciones = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    posicion = None
    while True:
        if posicion not in posiciones:
            posicion = input(f"{nombre} te toca (1-9) ")
        else:
            posicion = int(posicion)
            if not casilla_libre(tablero , posicion-1):
                print("esa posicion esta ocupada")
            else:
                return posicion-1
'''

'''
# se podria tener solo 1 , no se me ocurre una logica que funcione alintegrarlo
def movimiento_jugador2(tablero, nombre):
   
    posiciones = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    posicion = None
    while True:
        if posicion not in posiciones:
            posicion = input(f"{nombre}te toca (1-9) ")
        else:
            posicion = int(posicion)
            if not casilla_libre(tablero , posicion-1):
                print("esa posicion esta ocupada")
            else:
                return posicion-1
'''

def tablero_lleno(tablero):
    ''' devuelve true si el tablero esta lleno y false si hay casillas vacias, es la verificacion para no repetir una casilla'''
    for i in tablero:
        if i == " ":
            return False
        else:
            return True

def casilla_libre(tablero ,casilla):
    '''devuelve true si la casilla esta libre y false si ya esta llena '''
    return tablero[casilla] == " "
    
 # '''revisa si hay ganador con x'''
    # no pude crear uno solo para los dos casos. se tienen todas las posibles combinaciones con las que se puede ganar 
     #h

def hay_ganadorx(tablero, nombre):
    '''revisa si hay ganador con x'''

    if  tablero[0] == tablero[1] == tablero[2] == "X" or \
        tablero[3] == tablero[4] == tablero[5] == "X" or \
        tablero[6] == tablero[7] == tablero[8] == "X" or \
        tablero[0] == tablero[3] == tablero[6] == "X" or \
        tablero[1] == tablero[4] == tablero[7] == "X" or \
        tablero[2] == tablero[5] == tablero[8] == "X" or \
        tablero[0] == tablero[4] == tablero[8] == "X" or \
        tablero[2] == tablero[4] == tablero[6] == "X":
        print(col+ f"{nombre} tiene tres en fila. Ganaste")
        juegonuev()
        exit(0)
        return True
    else:
        return False

def hay_ganadoro(tablero,nombre):
    '''revisa si hay ganador '''

    if  tablero[0] == tablero[1] == tablero[2] == "O" or \
        tablero[3] == tablero[4] == tablero[5] == "O" or \
        tablero[6] == tablero[7] == tablero[8] == "O" or \
        tablero[0] == tablero[3] == tablero[6] == "O" or \
        tablero[1] == tablero[4] == tablero[7] == "O" or \
        tablero[2] == tablero[5] == tablero[8] == "O" or \
        tablero[0] == tablero[4] == tablero[8] == "O" or \
        tablero[2] == tablero[4] == tablero[6] == "O":
        print(col + f"{nombre} tiene tres en fila. Ganaste")
        juegonuev()
        exit(0)
        return True
    else:
        return False

def movimiento_jugadorO(tablero,nombre):
    '''se tiene un arreglo como tablero, donde se va a guardar el movimiento , se imprime en la posicion casilla en tablero la figura o '''
    campo=[" "]*9        
    campo=movimiento_jugador(tablero, nombre)
    tablero[campo]="O"
    mostrar_tablero(tablero) 
    os.system("cls") 
    mostrar_tablero(tablero)  
    '''se revisa si se tiene ina combinacion ganadora . si no sigue el juego y se boora pantalla '''
    hay_ganadoro(tablero,nombre)
    os.system("cls")
    mostrar_tablero(tablero)
    return campo

def movimiento_jugadorx(tablero,nombre):
    campo=[" "]*9
    campo=movimiento_jugador(tablero, nombre)
    tablero[campo]="X"
    mostrar_tablero(tablero)
    hay_ganadorx(tablero,nombre)
    os.system("cls")
    mostrar_tablero(tablero)
    return campo

def juegonuev():
    respuesta =  input(col +"    ¿otra partida (s)?").lower()
    if respuesta == "s" or respuesta == "si":
        tablero = [" "] *9
        eleccion_ficha(tablero)
    else:
        print(col +"Gracias por jugar")
        exit(0)
