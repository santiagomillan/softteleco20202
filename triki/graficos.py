
green = '\033[0;32m'
red = '\033[91m'
yellow = '\033[93m' 
#print( green + 'Hello')
lightblue='\033[94m'

def mostrar_tablero(tablero):

    '''muestra el tablero'''

    print()
    print(lightblue + "          TRIKI")
    print()
    print()
    print(lightblue + "     1          |2          |3")
    print(lightblue + "           {}    |     {}     |     {}".format(tablero[0], tablero[1], tablero[2]))
    print(lightblue + "                |           |      ")
    print(lightblue + "     ==================================")
    print(lightblue + "     4          |5          |6")
    print(lightblue + "           {}    |     {}     |     {}".format(tablero[3], tablero[4], tablero[5]))
    print(lightblue + "                |           |      ")
    print(lightblue + "     ==================================   ")
    print(lightblue + "     7          |8          |9")
    print(lightblue + "           {}    |     {}     |     {}".format(tablero[6], tablero[7], tablero[8]))
    print(lightblue + "                |           |        ")
    print()



def grafico_empate():
    print("")
    print(yellow +"▓▓▓")
    print(yellow +"▒▒▒▓▓")
    print(yellow +"▒▒▒▒▒▓")
    print(yellow +"▒▒▒▒▒▒▓")
    print(yellow +"▒▒▒▒▒▒▓")
    print(yellow +"▒▒▒▒▒▒▒▓")
    print(yellow +"▒▒▒▒▒▒▒▓▓▓")
    print(yellow +"▒▓▓▓▓▓▓░░░▓")
    print(yellow +"▒▓░░░░▓░░░░▓")
    print(yellow +"▓░░░░░░▓░▓░▓")
    print(yellow +"▓░░░░░░▓░░░▓")
    print(yellow +"▓░░▓░░░▓▓▓▓")
    print(yellow +"▒▓░░░░▓▒▒▒▒▓")
    print(yellow +"▒▒▓▓▓▓▒▒▒▒▒▓")
    print(yellow +"▒▒▒▒▒▒▒▒▓▓▓▓")
    print(yellow +"▒▒▒▒▒▓▓▓▒▒▒▒▓")
    print(yellow +"▒▒▒▒▓▒▒▒▒▒▒▒▒▓")
    print(yellow +"▒▒▒▓▒▒▒▒▒▒▒▒▒▓")
    print(yellow +"▒▒▓▒▒▒▒▒▒▒▒▒▒▒▓")
    print(yellow +"▒▓▒▓▒▒▒▒▒▒▒▒▒▓")
    print(yellow +"▒▓▒▓▓▓▓▓▓▓▓▓▓")
    print(yellow +"▒▓▒▒▒▒▒▒▒▓")
    print(yellow +"▒▒▓▒▒▒▒▒▓")
    print()
