import base64
from getpass import getpass
import json
import os
from registro import guardar_usuario
from crear_bookmark import cargar_book,nuevo_bookmk,crear_label,eliminar_book,editar_book,eliminar_label
import smtplib
#from pantalla_principal import menu_principal
#b64encode(contraseña)
#login_contraseña = login_contraseña.encode()
#b64encode(login_contraseña)
# funcion que verifica los datos ingresados y hace login

def inicio_sesion():
    login_name = input('digite su nombre de usuario: ')
    login_password = getpass('digite su contraseña: ')
    
    login_password = login_password.encode()
    login_password = base64.b64encode(login_password)
    login_password = login_password.decode()
    usuario = login_name
    with open('usuarios.json','r') as file1:
        d = json.loads(file1.read())
        file1.close()
        #print(d)
        for datos in d:
            #print(datos.keys())
            if login_name in datos.keys():
                datos[login_name]['password'] = datos[login_name]['password']
                #print(datos[login_name]['password'])
                if login_password in datos[login_name]['password']:   
                    #print(login_password)
                    #login_password = login_password.encode()
                    #login_password = base64.b64decode(login_password)
                    #login_password = login_password.decode()
                    #print(login_password)
                    print('sesion iniciada \n')
                    menu_principal(usuario)
                    #return login_name
                    
                else:
                    print('clave errornea')
                    #return inicio_sesion()
            else:
                print()
                #return inicio_sesion()
    return

def menu_principal(usuario):
    os.system('cls')    
    print('Digite 1 para Crear bookmark')
    print('Digite 2 para Editar bookmark')
    print('Digite 3 para Eliminar bookmark')
    print('Digite 4 para Ver bookmark')
    print('Digite 5 para Ver los bookmarks de un label')
    print('Digite 6 para Eliminar un label')
    print('Digite 7 para Editar label')
    print('Digite 8 para Crear un label')
    print('Digite 9 para salir de la actual cuenta')
    print("Digite 10 para salir del programa")

    menu=""
    menu = input()
    error = True
    while error:
        if menu =="1":
            nuevo_bookmk(usuario)
        if menu =="2":
            editar_book(usuario)
        if menu =="3":
            eliminar_book(usuario)
        if menu =="4":
            cargar_book()
        if menu =="5":
            print('falta filtrar por label')
        if menu =="6":
            print('error')
            #eliminar_label(usuario)
        if menu =="7":
            editar_book(usuario)
        if menu =="8":
            crear_label()
        if menu == "9":
            print("salir de cuenta")
            inicio_sesion()
        if menu =="10":
            exit()
        else:
            menu_principal(usuario)
            error = False
    return menu_principal(usuario)

#inicio_sesion()